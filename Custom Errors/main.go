package main

import (
	"errors"
	"log"
	"math/rand"
	"net/http"
	"time"
)

type RequestError struct {
	StatusCode int

	Err error
}

func CreateRequestError(code int, err error) *RequestError {
	return &RequestError{
		StatusCode: code,
		Err:        err,
	}
}

func (r *RequestError) Error() string {
	return r.Err.Error()
}

func randomBit() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(2)
}

func requestToDatabase() error {
	return errors.New("Some database error")
}

func logic() error {
	if randomBit() == 0 {
		return CreateRequestError(http.StatusTeapot, errors.New("Bad bit"))
	}
	err := requestToDatabase()
	if err != nil {
		return CreateRequestError(http.StatusBadRequest, err)
	}
	return nil
}

func handler(w http.ResponseWriter, req *http.Request) {
	err := logic()
	if err != nil {
		re, ok := err.(*RequestError)
		if ok {
			w.WriteHeader(re.StatusCode)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
	} else {
		w.WriteHeader(http.StatusOK)
	}
}

func main() {
	http.HandleFunc("/", handler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
