`docker run -p 5432:5432 --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres`

`go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest`

`~/go/bin/migrate create -ext sql -dir ./migrations/postgres -seq create_users_table`

`~/go/bin/migrate create -ext sql -dir ./migrations/postgres -seq create_links_table`

Допишем в миграции SQL-код ...

Создадим соответствующую таблицу:

`docker exec -it some-postgres bash`

`psql -U postgres`

`CREATE DATABASE hackernews;`

`~/go/bin/migrate -database postgres://postgres:mysecretpassword@/hackernews?sslmode=disable -path ./migrations/postgres up`

Можно откатить применённые миграции:

`~/go/bin/migrate -database postgres://postgres:mysecretpassword@/hackernews?sslmode=disable -path ./migrations/postgres down`

Или дропнуть всё, что лежит в бд:

`~/go/bin/migrate -database postgres://postgres:mysecretpassword@/hackernews?sslmode=disable -path ./migrations/postgres drop`