package main

import (
	"fmt"

	"github.com/gocolly/colly"
)

type Scraper struct {
	*colly.Collector
	maxDepth  int
	currDepth int
}

func main() {
	c := colly.NewCollector()
	s := Scraper{
		c,
		3,
		1,
	}

	s.OnHTML(".mw-parser-output", func(e *colly.HTMLElement) {
		links := e.ChildAttrs("a", "href")
		if s.currDepth <= s.maxDepth && len(links) > 0 {
			s.currDepth += 1
			e.Request.Visit(links[0])
		}
	})

	s.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	s.Visit("https://en.wikipedia.org/wiki/Go_(programming_language)")
}
