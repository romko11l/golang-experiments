package main

import (
	"fmt"
	"time"

	"github.com/roylee0704/gron"
)

func main() {
	c := gron.New()
	c.AddFunc(gron.Every(10*time.Second), func() { fmt.Println("QQ") })

	c.Start()
	time.Sleep(time.Minute * 1)
	c.Stop()
}
