package main

import (
	"fmt"
	"math/big"
)

func main() {
	a := new(big.Int)
	a.SetString("123456789123456789123456789123456789", 10)
	b := new(big.Int)
	b.SetString("987654321987654321987654321987654321", 10)
	c := new(big.Int)
	c.Add(a, b)
	fmt.Println(c)
}
