package main

import (
	"log"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// Generate cert and key
// openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out server.crt -keyout server.key

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.GET("/", func(c echo.Context) error {
		return c.HTML(http.StatusOK, `
			<h1>Welcome to Echo :)</h1>
		`)
	})

	s := http.Server{
		Addr:        ":8080",
		Handler:     e,
		ReadTimeout: 30 * time.Second,
	}

	err := s.ListenAndServeTLS("./server.crt", "./server.key")
	log.Fatal(err)
}
