## HowTo

Для взаимодействия с программой нужно поднять Postgresql:

`docker run --name postgres-docker -e POSTGRES_PASSWORD=PostgresPass -p 5432:5432 -d postgres`

-------------------------

## Source

https://www.alexedwards.net/blog/how-to-manage-database-timeouts-and-cancellations-in-go - статья с чуть более сложными примерами.