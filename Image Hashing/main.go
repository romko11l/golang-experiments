package main

import (
	"fmt"
	"image/jpeg"
	"image/png"
	"os"

	"github.com/corona10/goimagehash"
)

func main() {
	file1, _ := os.Open("cat.png")
	file2, _ := os.Open("cat.jpg")
	file3, _ := os.Open("pepe.jpg")
	file4, _ := os.Open("msu.jpg")
	file5, _ := os.Open("resized_cat.png")
	defer file1.Close()
	defer file2.Close()
	defer file3.Close()
	defer file4.Close()
	defer file5.Close()

	img1, _ := png.Decode(file1)
	img2, _ := jpeg.Decode(file2)
	img3, _ := jpeg.Decode(file3)
	img4, _ := jpeg.Decode(file4)
	img5, _ := png.Decode(file5)

	hash1, _ := goimagehash.AverageHash(img1)
	hash2, _ := goimagehash.AverageHash(img2)
	hash3, _ := goimagehash.AverageHash(img3)
	hash4, _ := goimagehash.AverageHash(img4)
	hash5, _ := goimagehash.AverageHash(img5)

	distance12, _ := hash1.Distance(hash2)
	distance23, _ := hash2.Distance(hash3)
	distance24, _ := hash2.Distance(hash4)
	distance15, _ := hash1.Distance(hash5)

	fmt.Println(distance12, "- distance between cat.png and cat.jpg")         // 0
	fmt.Println(distance23, "- distance between cat.jpg and pepe.jpg")        // 2
	fmt.Println(distance24, "- distance between cat.jpg and msu.jpg")         // 40
	fmt.Println(distance15, "- distance between cat.png and resized_cat.png") // 0
}
