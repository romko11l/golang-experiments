package main

import (
	"fmt"
	"time"
)

func sleep() {
	timer := time.AfterFunc(time.Second*5, func() {
		fmt.Println("Функция выполняется более 5 секунд")
	})
	defer timer.Stop()

	fmt.Println("Я сплю)")
	time.Sleep(time.Second * 10)
}

func main() {
	sleep()
}
