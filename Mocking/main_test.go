package main

import (
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
)

func TestTotalCost(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	columns := []string{"number", "cost"}
	mock.ExpectQuery("SELECT (.+) FROM products;").WillReturnRows(sqlmock.NewRows(columns).FromCSVString("2,3\n4,3\n6,2"))

	total, err := totalCost(db)
	if err != nil {
		t.Fatal(err)
	}

	if total != 30 {
		t.Errorf("totalCost return %d, expected %d", total, 30)
	}
}
