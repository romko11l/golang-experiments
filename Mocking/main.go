package main

import (
	"database/sql"

	_ "github.com/lib/pq"
)

// go test -race -v

func totalCost(db *sql.DB) (int, error) {
	res, err := db.Query("SELECT number, cost FROM products;")
	if err != nil {
		return 0, err
	}

	var number, cost, total int
	total = 0

	for res.Next() {
		err = res.Scan(&number, &cost)
		if err != nil {
			return 0, err
		}
		total += number * cost
	}

	err = res.Close()
	if err != nil {
		return 0, err
	}
	return total, nil
}

func main() {
}
