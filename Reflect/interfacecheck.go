package main

import (
	"fmt"
	"reflect"
)

type Cache interface {
	Get(key string) (string, error)
	Set(key string, info string) error
}

type MapCache struct {
	storage map[string]string
}

func (c MapCache) Get(key string) (string, error) {
	if val, ok := c.storage[key]; ok {
		return val, nil
	}
	return "", fmt.Errorf("no such key in storage")
}

func (c MapCache) Set(key string, info string) error {
	c.storage[key] = info
	return nil
}

func main() {
	cache := reflect.TypeOf((*MapCache)(nil)).Elem()
	inter := reflect.TypeOf((*Cache)(nil)).Elem()
	fmt.Println(cache.Implements(inter))

	c := MapCache{
		storage: make(map[string]string),
	}
	c.Set("123", "321")
	fmt.Println(c.Get("123"))
}
