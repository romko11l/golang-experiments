# HowTo

Этот код иллюстрирует взаимодействие через grpc go сервера и python клиента

`go run main.go` - запуск сервера

`python3 main.py` - запуск клиента

# Генерация go кода

`export PATH="$PATH:$(go env GOPATH)/bin"`

`protoc --go_out=./go-code/hello --go_opt=paths=source_relative --go-grpc_out=./go-code/hello --go-grpc_opt=paths=source_relative ./hello.proto`

# Генерация python кода

`python3 -m grpc_tools.protoc -I ./ --python_out=./python-code --grpc_python_out=./python-code ./hello.proto`