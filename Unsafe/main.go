package main

import (
	"fmt"
	"unsafe"
)

type SomeStruct struct {
	A int8
	B float64
	C int8
}

type AnotherStruct struct {
	A float64
	B int8
	C int8
}

func main() {
	var a1 int
	var a2 int8
	var a3 uint16

	fmt.Println("Int size:", unsafe.Sizeof(a1))
	fmt.Println("Int8 size:", unsafe.Sizeof(a2))
	fmt.Println("Uint8 size:", unsafe.Sizeof(a3))

	fmt.Println("Int pointer size:", unsafe.Sizeof(&a1))
	fmt.Println("Uint8 pointer size:", unsafe.Sizeof(&a3))

	str1 := SomeStruct{
		-1,
		45.23,
		22,
	}

	// вот почему иногда структуры хранятся не оптимально
	// т.е. A будто бы занимает 8 байт (но ему нужен всего 1 байт)
	fmt.Println("Offset example:", unsafe.Offsetof(str1.B))
	fmt.Println("Struct size:", unsafe.Sizeof(str1))

	// Здесь структура хранится более оптимально
	str2 := AnotherStruct{
		45.23,
		-1,
		22,
	}
	fmt.Println("Struct size:", unsafe.Sizeof(str2))

	// Арифметика указателей (привет сишникам)
	ptr1 := uintptr(unsafe.Pointer(&str2.B))
	ptr2 := uintptr(unsafe.Pointer(&str2)) + unsafe.Offsetof(str2.B)
	fmt.Println(ptr1 == ptr2)

	// Получили доступ к полю str2.B
	intptr1 := (*uint8)(unsafe.Pointer(ptr2))
	fmt.Println("Convert to uint8:", *intptr1)
	intptr2 := (*int8)(unsafe.Pointer(ptr2))
	fmt.Println("Convert to int8:", *intptr2)
}
