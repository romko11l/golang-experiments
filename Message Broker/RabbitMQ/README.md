# HowTO

- Запускаем контейнер с rabbitmq:

    `docker run -d --name rabbitmq -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password -p 15672:15672 -p 5672:5672 rabbitmq:3-management`

- Запуск producer-а и consumer-а:

    `go run producer.go`

    `go run consumer.go`

 - Можно посмотреть статистику для очереди сообщений: `http://localhost:15672/#/`