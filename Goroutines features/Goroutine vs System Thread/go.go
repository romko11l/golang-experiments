package main

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

func main() {
	fmt.Println("ps -T -p " + strconv.Itoa(os.Getpid()))
	for i := 0; i < 100; i++ {
		go func() {
			time.Sleep(time.Minute * 10)
		}()
	}
	time.Sleep(time.Minute * 11)
}
