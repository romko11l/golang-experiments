package main

//#include <unistd.h>
import "C"
import (
	"fmt"
	"os"
	"strconv"
	"time"
)

func main() {
	fmt.Println("ps -T -p " + strconv.Itoa(os.Getpid()))
	for i := 0; i < 100; i++ {
		go func() {
			C.sleep(60 * 10)
		}()
	}
	time.Sleep(11 * time.Minute)
}
