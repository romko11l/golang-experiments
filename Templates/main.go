package main

import (
	"html/template"
	"io"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Renderer struct {
	template *template.Template
	debug    bool
	location string
}

func NewRenderer(location string, debug bool) *Renderer {
	tpl := &Renderer{
		debug:    debug,
		location: location,
	}
	tpl.ReloadTemplates()
	return tpl
}

func (t *Renderer) ReloadTemplates() {
	t.template = template.Must(template.ParseGlob(t.location))
}

func (t *Renderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	if t.debug {
		t.ReloadTemplates()
	}

	return t.template.ExecuteTemplate(w, name, data)
}

func main() {
	e := echo.New()

	e.Renderer = NewRenderer("./*.html", true)

	e.GET("/", homePage)
	e.GET("/:id", idPage)

	e.Logger.Fatal(e.Start(":8080"))
}

func homePage(c echo.Context) error {
	data := map[string]interface{}{
		"message": "Hello",
	}
	return c.Render(http.StatusOK, "home.html", data)
}

func idPage(c echo.Context) error {
	data := map[string]interface{}{
		"message": "Your id is",
		"id":      c.Param("id"),
	}
	return c.Render(http.StatusOK, "id.html", data)
}
