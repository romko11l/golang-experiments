package main

import (
	post "easy/Post"
	"fmt"
	"log"
)

func main() {
	somePost := post.Post{
		ID:       "abcd-efgh",
		AuthorID: "1234-5678",
		Text:     "Hello, i`am simple struct",
	}
	b, err := somePost.MarshalJSON()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(b)

	newPost := post.Post{}
	err = newPost.UnmarshalJSON(b)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(newPost)
}
