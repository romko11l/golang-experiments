package post

//easyjson:json
type Post struct {
	ID       string `json:"Id"`
	AuthorID string `json:"AuthorId"`
	Text     string `json:"Text"`
}
