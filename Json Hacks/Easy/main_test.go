package main

import (
	post "easy/Post"
	"encoding/json"
	"log"
	"testing"
)

type NotEasyPost struct {
	ID       string `json:"Id"`
	AuthorID string `json:"AuthorId"`
	Text     string `json:"Text"`
}

func BenchmarkPost(b *testing.B) {
	benchPost := NotEasyPost{
		ID:       "abcd-efgh",
		AuthorID: "1234-5678",
		Text:     "Hello, i`am simple struct",
	}
	for i := 0; i < b.N; i++ {
		data, err := json.Marshal(benchPost)
		if err != nil {
			log.Fatal(err)
		}
		err = json.Unmarshal(data, &NotEasyPost{})
		if err != nil {
			log.Fatal(err)
		}
	}
}

func BenchmarkEasyPost(b *testing.B) {
	benchPost := post.Post{
		ID:       "abcd-efgh",
		AuthorID: "1234-5678",
		Text:     "Hello, i`am simple struct",
	}
	for i := 0; i < b.N; i++ {
		data, err := benchPost.MarshalJSON()
		if err != nil {
			log.Fatal(err)
		}
		err = benchPost.UnmarshalJSON(data)
		if err != nil {
			log.Fatal(err)
		}
	}
}
