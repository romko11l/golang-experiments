package main

import (
	"fmt"
	"log"

	"github.com/valyala/fastjson"
)

func main() {
	var p fastjson.Parser
	s := `{"ID": "abcd-efgh", "AuthorID": "1234-5678", "Text": "Hello, i am simple struct"}`
	v, err := p.Parse(s)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("ID = %s\n", v.GetStringBytes("ID"))
	fmt.Printf("AuthorID = %s\n", v.GetStringBytes("AuthorID"))
	fmt.Printf("Text = %s\n", v.GetStringBytes("Text"))
}
